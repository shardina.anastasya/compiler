﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace binHeap
{
    class BinHeap<T> where T : IComparable<T>
    {
        private List<T> list = new List<T>();

        public int heapSize
        {
            get
            {
                return this.list.Count();
            }
        }

        public void add(T value)
        {
            list.Add(value);
            int i = heapSize - 1;
            int parent = (i - 1) / 2;

            while (i > 0 && (list[parent].CompareTo(list[i]) < 0))
            {
                T temp = list[i];
                list[i] = list[parent];
                list[parent] = temp;

                i = parent;
                parent = (i - 1) / 2;
            }
        }

        private void heapify(int i)
        {
            int leftChild;
            int rightChild;
            int largestChild;

            for (; ; )
            {
                leftChild = 2 * i + 1;
                rightChild = 2 * i + 2;
                largestChild = i;

                if (leftChild < heapSize && list[leftChild].CompareTo(list[largestChild]) > 0)
                {
                    largestChild = leftChild;
                }

                if (rightChild < heapSize && list[rightChild].CompareTo(list[largestChild]) > 0)
                {
                    largestChild = rightChild;
                }

                if (largestChild == i)
                {
                    break;
                }

                T temp = list[i];
                list[i] = list[largestChild];
                list[largestChild] = temp;
                i = largestChild;
            }
        }

        public void buildHeap(T[] sourceArray)
        {
            list = sourceArray.ToList();
            for (int i = heapSize / 2; i >= 0; i--)
            {
                heapify(i);
               // print();
            }
        }

        public T getMax()
        {
            return list[0];
        }

        public T delMax()
        {
            T result = list[0];
            list[0] = list[heapSize - 1];
            list.RemoveAt(heapSize - 1);
            heapify(0);
            return result;
        }

        public void print()
        {
            int countLvl = (int)(Math.Log(heapSize) / Math.Log(2));
            int count = 1, curInd = 0;

            for(int i = 0; i <= countLvl; i++)
            {
                int j = 0;
                while (j < count && curInd < heapSize)
                {
                    Console.Write(list[curInd] + "    ");
                    curInd++;
                    j++;
                }

                count *= 2;
                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }

   public class Program
    {
        public static double M;
        public static List<Thing> lstThings;

        static void Main(string[] args)
        {
            int n;
            string s;
            lstThings = new List<Thing>();
            using (StreamReader sr = new StreamReader("test.txt"))
            {
                M = Convert.ToDouble(sr.ReadLine());
                n = int.Parse(sr.ReadLine());
                for (int i = 1; i <= n; i++)
                {
                    s = sr.ReadLine();
                    var arr = s.Split(' ');
                    Thing th = new Thing(Convert.ToDouble(arr[0]), Convert.ToDouble(arr[1]), i);
                    lstThings.Add(th);
                }
            }
            lstThings.Sort((o1,o2)=>-o1.val.CompareTo(o2.val));
            int lvlCount = n;
            BinHeap<Vertex> heap = new BinHeap<Vertex>();
            Vertex root = new Vertex(0); //создали корень дерева
            heap.add(root);
            int lvl;
            Vertex vMax;
            while((vMax = heap.delMax()).h != n)
            {
                lvl = vMax.h;
                Vertex vLeft = new Vertex(lvl+1, lstThings[lvl], vMax);
                Vertex vRight = new Vertex(lvl+1, vMax);
                if(vLeft.sumMass <= M) heap.add(vLeft);
                if (vRight.sumMass <= M) heap.add(vRight);
            }
            vMax.PrintVertex();

        }
    }
}
