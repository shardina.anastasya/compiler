﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Compiler
{
    struct TextPosition
    {
        public int numberLine;
        public int position;
    }

    class Compiler
    {
        List<Error> errors = new List<Error>();
        List<ErrorPosition> errorsPositions = new List<ErrorPosition>();
        string sourceFilePath;
        string resultFilePath;
        string buf = "";
        Litera litera;
        TextPosition textPosition;
        bool emptyBuf = true;
        StreamReader sr;
        StreamWriter sw;
        string errorsPath = "Errors.txt";
        int maxint = 32767;
        int minint = -32768;
        double maxreal = 1.7 * Math.Pow(10, 38);
        double minreal = 2.9 * Math.Pow(10, -39);
        Symbol symbol;
        TextPosition token;

        int numPrintLine = 0;
        int numberError = 1;

        public Compiler(string sourceFilePath, string resultFilePath, List<Error> errors)
        {
            this.sourceFilePath = sourceFilePath;
            this.resultFilePath = resultFilePath;
            this.errors = errors;
        }

        private void Error(string code, TextPosition pos)
        {
            var index = errors.FindIndex((o) => o.code == code);
            Error error = errors[index];
            ErrorPosition errorPos = new ErrorPosition(pos.numberLine,
                pos.position, error);
            errorsPositions.Add(errorPos);
        }

        private void ListLine()
        {
            int numline = textPosition.numberLine - 1;
            if (numline != numPrintLine)
            {
                numPrintLine = numline;
                if (errorsPositions.Count > 0)
                {
                    ListErrors(numline - 1);
                }

                sw.WriteLine("{0,4}  {1}", numline, oldbuf);
            }
        }

        private void ListErrors()
        {
            for (int i = 0; i < errorsPositions.Count; i++)
            {
                if (errorsPositions[i].numberLine == textPosition.numberLine-1)
                {
                    string numbError = (numberError).ToString();
                    numberError++;
                    numbError = numbError.PadLeft(2, '0');
                    sw.WriteLine("**{0}**{1," + errorsPositions[i].position + "}^ ошибка  код {2,6}",
                        numbError, "", errorsPositions[i].error.code);
                    sw.WriteLine("****** {0}", errorsPositions[i].error.description);

                    //countPrintErrors++;
                    errorsPositions.RemoveAt(i);
                    i--;
                }
            }
        }

        private void ListErrors(int line)
        {
            for (int i = 0; i < errorsPositions.Count; i++)
            {
                if (errorsPositions[i].numberLine == line)
                {
                    string numbError = (numberError).ToString();
                    numbError = numbError.PadLeft(2, '0');
                    sw.WriteLine("**{0}**{1," + errorsPositions[i].position + "}^ ошибка  код {2,6}",
                        numbError, "", errorsPositions[i].error.code);
                    sw.WriteLine("****** {0}", errorsPositions[i].error.description);

                    //countPrintErrors++;
                    errorsPositions.RemoveAt(i);
                    i--;
                    numberError++;

                }
            }
        }

        private void ListErrors(int line, int count)
        {
            int ind = -1;

            for (int i = 0; i < errorsPositions.Count; i++)
            {
                if (errorsPositions[i].numberLine == line)
                {
                    ind = i;
                    break;
                }
            }
            if (ind != -1 && ind < errorsPositions.Count)
            {
                ind += count;


                while (ind < errorsPositions.Count && errorsPositions[ind].numberLine == line)
                {
                    string numbError = (ind + 1).ToString();
                    numbError = numbError.PadLeft(2, '0');
                    sw.WriteLine("**{0}**{1," + errorsPositions[ind].position + "}^ ошибка  код {2,6}",
                        numbError, "", errorsPositions[ind].error.code);
                    sw.WriteLine("****** {0}", errorsPositions[ind].error.description);

                    countPrintErrors++;
                    ind++;
                }
            }
        }

        private bool HasErrors()
        {
            int numLine = textPosition.numberLine-1;
            var index = errorsPositions.FindIndex((o) => o.numberLine == numLine);
            if (index != -1)
                return true;
            else return false;
        }

        string oldbuf;
        int countErrors = 0;
        int countPrintErrors = 0;

        private Litera NextCh()
        {
            if (buf.Length == 0 || textPosition.position == buf.Length - 1)
            {
                oldbuf = buf;
                if (!sr.EndOfStream)
                {
                    buf = sr.ReadLine();
                    textPosition.numberLine++;
                    textPosition.position = 0;
                }
                else
                    emptyBuf = true;
            }
            else if(textPosition.position == 0 && textPosition.numberLine != 1)
            {
                ListLine();
                if (HasErrors())
                    ListErrors();
                textPosition.position++;
            }
            else
                textPosition.position++;

                if (!emptyBuf)
                litera = new Litera(buf[textPosition.position], textPosition.numberLine,
                    textPosition.position);
                else
                litera = null;

            return litera;
        }

        private bool isLetter(char ch)
        {
            if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch == '_')
                return true;
            else return false;
        }

        private bool isDigit(char ch)
        {
            if (ch >= '0' && ch <= '9')
                return true;
            else return false;
        }
        private Symbol NextSym()
        {

            int sym = -1;
            while (!emptyBuf && litera.ch == ' ') NextCh();
            if (emptyBuf)
                return null;
            token.numberLine = textPosition.numberLine;
            token.position = textPosition.position;

            if (isLetter(litera.ch))
            {
                string name = "";
                while (litera != null && token.numberLine == textPosition.numberLine && (isDigit(litera.ch) || isLetter(litera.ch)))
                {
                    name += litera.ch;
                    NextCh();
                }

                sym = Keys.searchSym(name);
            }
            else if (isDigit(litera.ch))
            {
                string digit = "";
                double x = 0;
                bool end = false;
                while (litera != null && token.numberLine == textPosition.numberLine && (isDigit(litera.ch) || litera.ch == '.' || litera.ch == '-' ||
                    litera.ch == '+' || litera.ch == 'e' || litera.ch == 'E') /*&& textPosition.position < buf.Length - 1*/)
                {
                    digit += litera.ch;
                    NextCh();
                }

                //if ((isDigit(litera.ch) || litera.ch == '.' || litera.ch == '-' ||
                //    litera.ch == '+' || litera.ch == 'e' || litera.ch == 'E'))
                //{
                //    if (textPosition.position == buf.Length - 1)
                //        end = true;
                //    digit += litera.ch;
                //}

                try
                {
                    x = Convert.ToDouble(digit, CultureInfo.InvariantCulture);

                    if (x % 1 == 0)
                    {
                        sym = (int)Keys.Key.intc;
                        if (x > maxint) Error("203", token);

                    }
                    else
                    {
                        sym = (int)Keys.Key.floatc;
                        if (x > maxreal) Error("207", token);
                        else if (x < minreal) Error("206", token);
                    }
                }
                catch
                {
                    decimal d;
                    decimal.TryParse(digit, NumberStyles.Float, CultureInfo.InvariantCulture, out d);
                    if (d == 0)
                    {
                        Error("201", token);
                        sym = (int)Keys.Key.floatc;
                    }
                }

               // if (end)
               //     NextCh();
            }
            else
                switch (litera.ch)
                {
                    case '\'':
                        string constant = "";
                        NextCh();
                        while (litera != null && litera.ch != '\'')
                        {
                            constant += litera.ch;
                            NextCh();
                        }
                        var arr = constant.ToCharArray();
                        if (arr.Length == 0 || arr.Length > 1)
                            Error("75", token);
                        sym = (int)Keys.Key.stringc;
                        NextCh();
                        break;
                    case ':': NextCh();
                        if (litera != null && litera.ch == '=')
                        {
                            sym = (int)Keys.Key.assign;
                            NextCh();
                        }
                        else
                            sym = (int)Keys.Key.colon;
                        break;
                    case '<': NextCh();
                        if (litera != null && litera.ch == '=')
                        {
                            sym = (int)Keys.Key.laterequal;
                            NextCh();
                        }
                        else if (litera != null && litera.ch == '>')
                        {
                            sym = (int)Keys.Key.latergreater;
                            NextCh();
                        }
                        else
                            sym = (int)Keys.Key.later;
                        break;
                    case '>': NextCh();
                        if (litera != null && litera.ch == '=')
                        {
                            sym = (int)Keys.Key.greaterequal;
                            NextCh();
                        }
                        else
                            sym = (int)Keys.Key.greater;
                        break;
                    case '+':
                        sym = (int)Keys.Key.plus;
                        NextCh();
                        break;
                    case '-':
                        sym = (int)Keys.Key.minus;
                        NextCh();
                        break;
                    case ';':
                        sym = (int)Keys.Key.semicolon;
                        NextCh();
                        break;
                    case '^':
                        sym = (int)Keys.Key.arrow;
                        NextCh();
                        break;
                    case '[':
                        sym = (int)Keys.Key.lbracket;
                        NextCh();
                        break;
                    case ']':
                        sym = (int)Keys.Key.rbracket;
                        NextCh();
                        break;
                    case '{':
                        sym = (int)Keys.Key.flpar;
                        NextCh();
                        while (litera != null && litera.ch != '}')
                            NextCh();
                        NextCh();
                        return NextSym();
                        break;
                    case '}':
                        sym = (int)Keys.Key.frpar;
                        NextCh();
                        break;
                    case '=':
                        sym = (int)Keys.Key.equal;
                        NextCh();
                        break;
                    case '/':
                        sym = (int)Keys.Key.slash;
                        NextCh();

                        break;
                    case '.':
                        NextCh();
                        if (litera != null && litera.ch == '.')
                        {
                            sym = (int)Keys.Key.twopoints;
                            NextCh();
                        }
                        else
                            sym = (int)Keys.Key.point;
                        break;
                    case ',':
                        sym = (int)Keys.Key.comma;
                        NextCh();
                        break;
                    case '*':
                        NextCh();
                        if (litera != null && litera.ch == ')')
                        {
                            sym = (int)Keys.Key.rcomment;
                            NextCh();
                        }
                        else
                            sym = (int)Keys.Key.star;
                        break;
                    case '(':
                        NextCh();
                        if (litera != null && litera.ch == '*')
                        {
                            sym = (int)Keys.Key.lcomment;
                            NextCh();
                            while (litera != null && litera.ch != '*' && !emptyBuf)
                                NextCh();
                            NextCh();
                            do
                            {
                                if (litera != null && litera.ch == ')' && !emptyBuf)
                                {
                                    sym = (int)Keys.Key.rcomment;
                                }
                                else
                                {
                                    while (litera != null && litera.ch != '*' && !emptyBuf)
                                        NextCh();
                                    NextCh();
                                }
                            } while (litera != null && litera.ch != ')' && !emptyBuf);
                            sym = (int)Keys.Key.rcomment;
                            NextCh();
                            return NextSym();
                        }
                        else
                        { sym = (int)Keys.Key.leftpar;
                        }
                        break;
                    case ')':
                        sym = (int)Keys.Key.rightpar;
                        NextCh();
                        break;
                    default: Error("6", token);
                        NextCh();
                        return NextSym();
                        break;

                }

            int line = token.numberLine;
            int pos = token.position;
            if (emptyBuf)
            {
                textPosition.numberLine++;
                ListLine();
                if (HasErrors())
                    ListErrors();
            }
            symbol = new Symbol(sym, line, pos);
            return new Symbol(sym, line, pos);
        }

        private void accept(int expectedcode)
        {

            if (symbol.code == expectedcode)
            {
                NextSym();
            }
            else
            {
                Error(expectedcode.ToString(), token);
            }
        }

        private void programme()
        {
            accept((int)Keys.Key.programsy);
            accept((int)Keys.Key.ident);
            if(symbol.code == (int)Keys.Key.leftpar)
            {
                do
                {
                    NextSym();
                    accept((int)Keys.Key.ident);
                }
                while (symbol.code == (int)Keys.Key.comma);
                accept((int)Keys.Key.rightpar);
            }
            accept((int)Keys.Key.semicolon);
            block(/*new HashSet<int>() { (int)Keys.Key.point }*/);    
            accept((int)Keys.Key.point);
        }

        HashSet<int> begpart;
        HashSet<int> typeP;
        HashSet<int> varP;
        HashSet<int> procfuncpart;
        HashSet<int> starters;
        HashSet<int> afterVar;

        private void initialSets()
        {
            begpart = new HashSet<int>() { (int)Keys.Key.labelsy, (int)Keys.Key.constsy, (int)Keys.Key.typesy,
            (int)Keys.Key.varsy, (int)Keys.Key.functionsy, (int)Keys.Key.proceduresy, (int)Keys.Key.beginsy};
            typeP = new HashSet<int>() { (int)Keys.Key.typesy, (int)Keys.Key.varsy, (int)Keys.Key.functionsy,
            (int)Keys.Key.proceduresy, (int)Keys.Key.beginsy};
            varP = new HashSet<int>() { (int)Keys.Key.varsy, (int)Keys.Key.functionsy, (int)Keys.Key.proceduresy,
            (int)Keys.Key.beginsy};
            procfuncpart = new HashSet<int>() { (int)Keys.Key.functionsy, (int)Keys.Key.proceduresy,
            (int)Keys.Key.beginsy};
            starters = new HashSet<int>() { (int)Keys.Key.ident };
            afterVar = new HashSet<int>() { (int)Keys.Key.semicolon };
        }

        private void skipto2(HashSet<int> set1, HashSet<int> set2)
        {
            while (!emptyBuf && !set1.Contains(symbol.code) && !set2.Contains(symbol.code))
                NextSym();
        }

        private void skipto(HashSet<int> set)
        {
            while (!emptyBuf && !set.Contains(symbol.code))
                NextSym();
        }

        private void block(/*HashSet<int> followers*/)
        {
            //HashSet<int> ptra;
            //if(!belong(symbol.code, begpart))
            //{
            //    Error("18", token);
            //    skipto2(begpart, followers);
            //}
            //if(belong(symbol.code, begpart))
            //{
            //    constpart();
            //    ptra = disjunct(varP, followers);
            //    //typepart(ptra);
            //    ptra = disjunct(procfuncpart, followers);
            //    varpart(ptra);
            //    //procpart(ptra);
            //    //statementpart(followers);
            //    if(!belong(symbol.code, followers))
            //    {
            //        Error("6", token);
            //        skipto(followers);
            //    }

            //}
            constpart();
            typepart();
            varpart();
            procpart();
            statementpart();
        }

        private void typepart()
        {
            if(symbol.code == (int)Keys.Key.typesy)
            {
                accept((int)Keys.Key.typesy);
                typedefinition();
                accept((int)Keys.Key.semicolon);
                while (symbol.code == (int)Keys.Key.ident)
                {
                    typedefinition();
                    accept((int)Keys.Key.semicolon);
                }
            }
        }

        private void typedefinition()
        {
            accept((int)Keys.Key.ident);
            accept((int)Keys.Key.equal);
            type();
        }

        private void type()
        {
            simpletype();
        }

        private void simpletype()
        {
            switch (symbol.code)
            {
                case (int)Keys.Key.leftpar:
                    accept((int)Keys.Key.leftpar);
                    do
                    {
                        accept((int)Keys.Key.ident);
                    }
                    while (symbol.code == (int)Keys.Key.comma);
                    accept((int)Keys.Key.rightpar);
                    break;
                case (int)Keys.Key.intc:
                    accept((int)Keys.Key.intc);
                    accept((int)Keys.Key.twopoints);
                    accept((int)Keys.Key.intc);
                    break;
                case (int)Keys.Key.floatc:
                    accept((int)Keys.Key.floatc);
                    accept((int)Keys.Key.twopoints);
                    accept((int)Keys.Key.floatc);
                    break;
                case (int)Keys.Key.ident:
                    accept((int)Keys.Key.ident);
                    if(symbol.code == (int)Keys.Key.twopoints)
                    {
                        accept((int)Keys.Key.twopoints);
                        accept((int)Keys.Key.ident);
                    }
                    break;
            }
            
        }

        private void constpart()
        {
            if (symbol.code == (int)Keys.Key.constsy)
            {
                accept((int)Keys.Key.constsy);
                do
                {
                    descriptConst();
                    accept((int)Keys.Key.semicolon);
                }
                while (symbol.code == (int)Keys.Key.ident);
            }
        }

        private void descriptConst() {
            accept((int)Keys.Key.ident);
            accept((int)Keys.Key.equal);
            constt();
        }

        private void constt()
        {
            if (symbol.code == (int)Keys.Key.plus)
            {
                accept((int)Keys.Key.plus);
                if (symbol.code == (int)Keys.Key.ident)
                    accept((int)Keys.Key.ident);
                else
                    digit();
            }
            else if (symbol.code == (int)Keys.Key.minus)
            {
                accept((int)Keys.Key.minus);
                if (symbol.code == (int)Keys.Key.ident)
                    accept((int)Keys.Key.ident);
                else
                    digit();
            }
            else if (symbol.code == (int)Keys.Key.ident)
            {
                accept((int)Keys.Key.ident);
            }
            else
            {
                digit();
            }
        }

        private void digit()
        {
            if (symbol.code == (int)Keys.Key.intc)
                accept((int)Keys.Key.intc);
            else
                accept((int)Keys.Key.floatc);
        }

        private void assignstatement() //?????
        {
            accept((int)Keys.Key.ident);
            accept((int)Keys.Key.assign);
            expression();
        }

        private void procpart()
        {
            while (symbol.code == (int)Keys.Key.proceduresy)
            {
                descrproc();
                accept((int)Keys.Key.semicolon);
            }
        }

        private void descrproc()
        {
            titleproc();
            block();
        }

        private void expression()
        {
            simpleexpr();
            if (symbol.code == (int)Keys.Key.latergreater || symbol.code == (int)Keys.Key.equal ||
                symbol.code == (int)Keys.Key.later || symbol.code == (int)Keys.Key.laterequal ||
                symbol.code == (int)Keys.Key.greaterequal || symbol.code == (int)Keys.Key.greater ||
                symbol.code == (int)Keys.Key.insy)
            {
                NextSym();
                simpleexpr();
            }
        }

        private void simpleexpr()
        {
            switch (symbol.code)
            {
                case (int)Keys.Key.plus: accept((int)Keys.Key.plus); break;
                case (int)Keys.Key.minus: accept((int)Keys.Key.minus); break;
            }

            addend();

            while(symbol.code == (int)Keys.Key.plus || symbol.code == (int)Keys.Key.minus||
                symbol.code == (int)Keys.Key.orsy)
            {
                NextSym();
                addend();
            }
        
        }

        private void addend()
        {
            multiplier();
            while (symbol.code == (int)Keys.Key.star || symbol.code == (int)Keys.Key.slash ||
                symbol.code == (int)Keys.Key.divsy || symbol.code == (int)Keys.Key.modsy ||
                symbol.code == (int)Keys.Key.andsy)
            {
                NextSym();
                multiplier();
            }
        }

        private void multiplier()
        {
            switch(symbol.code)
            {
                case (int)Keys.Key.ident:
                    accept((int)Keys.Key.ident);
                    break;
                case (int)Keys.Key.nilsy:
                    accept((int)Keys.Key.nilsy);
                    break;
                case (int)Keys.Key.intc:
                    accept((int)Keys.Key.intc);
                    break;
                case (int)Keys.Key.floatc:
                    accept((int)Keys.Key.floatc);
                    break;
                case (int)Keys.Key.stringc:
                    accept((int)Keys.Key.stringc);
                    break;
                case (int)Keys.Key.leftpar:
                    accept((int)Keys.Key.leftpar);
                    expression();
                    accept((int)Keys.Key.rightpar);
                    break;
                case (int)Keys.Key.lbracket:
                    accept((int)Keys.Key.lbracket);
                    variety();
                    break;
                case (int)Keys.Key.notsy:
                    accept((int)Keys.Key.notsy);
                    multiplier();
                    break;
            }
        }

        private void variety()
        {
            accept((int)Keys.Key.lbracket);
            if (symbol.code == (int)Keys.Key.plus || symbol.code == (int)Keys.Key.minus)
            {
                do
                {
                    element();
                }
                while (symbol.code == (int)Keys.Key.comma);
            }
            accept((int)Keys.Key.rbracket);
        }

        private void element()
        {
            expression();
            if(symbol.code == (int)Keys.Key.twopoints)
            {
                NextSym();
                expression();
            }
        }

        private void titleproc()
        {
            accept((int)Keys.Key.proceduresy);
            accept((int)Keys.Key.ident);
            if (symbol.code == (int)Keys.Key.leftpar)
            {
                do
                {
                    NextSym();
                    formalparam();
                }
                while(symbol.code == (int)Keys.Key.semicolon);

                accept((int)Keys.Key.rightpar);
            }
            accept((int)Keys.Key.semicolon);
        }

        private void formalparam()
        {
            switch (symbol.code)
            {
                case (int)Keys.Key.varsy:
                    accept((int)Keys.Key.varsy);
                    groupparam();
                    break;
                case (int)Keys.Key.functionsy:
                    accept((int)Keys.Key.functionsy);
                    groupparam();
                    break;
                case (int)Keys.Key.proceduresy:
                    do
                    {
                        accept((int)Keys.Key.ident);
                    } while (symbol.code == (int)Keys.Key.comma);
                    break;
                default:
                    groupparam();
                    break;
            }
        }

        private void groupparam()
        {
            do
            {
                accept((int)Keys.Key.ident);
            } while (symbol.code == (int)Keys.Key.comma);
            accept((int)Keys.Key.colon);
            accept((int)Keys.Key.ident);
        }

        private void whilestatement()
        {
            accept((int)Keys.Key.whilesy);
            accept((int)Keys.Key.dosy);
        }
        
        private void repeatstatement()
        {
            accept((int)Keys.Key.repeatsy);
            statement();
            while (symbol.code == (int)Keys.Key.semicolon)
            {
                NextSym();
                statement();
            }
            accept((int)Keys.Key.untilsy);
            expression();
        }

        private void forstatement()
        {
            accept((int)Keys.Key.forsy);
            accept((int)Keys.Key.ident);
            accept((int)Keys.Key.assign);
            expression(); //выражение
            if (symbol.code == (int)Keys.Key.tosy || symbol.code == (int)Keys.Key.downtosy)
                NextSym();
            expression();
            accept((int)Keys.Key.dosy);
            statement();
        }

        private void statementpart()
        {
            compoundstatement();
        }

        private void compoundstatement()
        {
            accept((int)Keys.Key.beginsy);
            statement();
            while (symbol.code == (int)Keys.Key.semicolon)
            {
                NextSym();
                statement();
            }
            accept((int)Keys.Key.endsy);
        }

        private void statement()
        {
            if (symbol.code == (int)Keys.Key.intc)
            {
                accept((int)Keys.Key.intc);
                unlabeledstatement();
            }
            else
                unlabeledstatement();
        }

        private void unlabeledstatement()
        {
            if(symbol.code == (int)Keys.Key.beginsy || symbol.code == (int)Keys.Key.ifsy ||
                symbol.code == (int)Keys.Key.repeatsy || symbol.code == (int)Keys.Key.forsy ||
                symbol.code == (int)Keys.Key.whilesy)
                 complicatedstatement();
            else
            simplestatement();
        }

        private void simplestatement()
        {
            if (symbol.code == (int)Keys.Key.ident)
            {
                accept((int)Keys.Key.ident);
                switch (symbol.code)
                {
                    case (int)Keys.Key.assign:
                        accept((int)Keys.Key.assign);
                        expression();
                        break;
                    case (int)Keys.Key.leftpar:
                        do
                        {
                            NextSym();
                            factparam();
                        }
                        while (symbol.code == (int)Keys.Key.comma);
                        accept((int)Keys.Key.rightpar);
                        break;
                }
            }
        }

        private void factparam()
        {
            if (symbol.code == (int)Keys.Key.floatc || symbol.code == (int)Keys.Key.intc)
                expression();
            else
            if (symbol.code == (int)Keys.Key.plus || symbol.code == (int)Keys.Key.minus)
                expression();
            else
                accept((int)Keys.Key.ident);
        }

        private void complicatedstatement()
        {
            switch (symbol.code)
            {
                case (int)Keys.Key.beginsy:
                    compoundstatement();
                    break;
                case (int)Keys.Key.ifsy:
                    ifstatement();
                    break;
                case (int)Keys.Key.repeatsy:
                    repeatstatement();
                    break;
                case (int)Keys.Key.forsy:
                    forstatement();
                    break;
                case (int)Keys.Key.whilesy:
                    whilestatement();
                    break;
            }
        }

        private void varpart(/*HashSet<int> followers*/)
        {
            //HashSet<int> ptra;
            //if(!belong(symbol.code, varP))
            //{
            //    Error("18", token);
            //    skipto2(varP, followers);
            //}

            //if (symbol.code == (int)Keys.Key.varsy)
            //{
                accept((int)Keys.Key.varsy);
               // ptra = disjunct(afterVar, followers);
                do
                {
                    vardeclaration(/*ptra*/);
                    accept((int)Keys.Key.semicolon);
                }
                while (symbol.code == (int)Keys.Key.ident);

                //if(!belong(symbol.code, followers))
                //{
                //    Error("6", token);
                //    skipto(followers);
                //}

           // }
        }

        private void vardeclaration(/*HashSet<int> followers*/)
        {
            //if(!belong(symbol.code, starters))
            //{
            //    Error("2", token);
            //    skipto2(starters, followers);
            //}

            accept((int)Keys.Key.ident);
            while (symbol.code == (int)Keys.Key.comma)
            {
                NextSym();
                accept((int)Keys.Key.ident);
            }
            accept((int)Keys.Key.colon);
            type(/*followers*/);

            //if(!belong(symbol.code, followers))
            //{
            //    Error("6", token);
            //    skipto(followers);
            //}
        }

        private void variable()
        {
            accept((int)Keys.Key.ident);
            while (symbol.code == (int)Keys.Key.lbracket || symbol.code == (int)Keys.Key.point||
                symbol.code == (int)Keys.Key.arrow)
                switch (symbol.code)
                {
                    case (int)Keys.Key.lbracket:
                        NextSym();
                        expression();
                        while(symbol.code == (int)Keys.Key.comma)
                        {
                            NextSym();
                            expression();
                        }
                        accept((int)Keys.Key.rbracket);
                        break;
                    case (int)Keys.Key.point:
                        NextSym();
                        accept((int)Keys.Key.ident);
                        break;
                    case (int)Keys.Key.arrow: NextSym(); break;
                }
        }

        private void ifstatement()
        {
            accept((int)Keys.Key.ifsy);
            expression();
            accept((int)Keys.Key.thensy);
            statement();
            if(symbol.code == (int)Keys.Key.elsesy)
            {
                NextSym();
                statement();
            }
        }

        private bool belong(int codeElement, HashSet<int> set)
        {
            return set.Contains(codeElement);
        }

        private HashSet<int> disjunct(HashSet<int> set1, HashSet<int> set2)
        {
            set1.UnionWith(set2);
            return set1;
        }


        public void Start()
        {
            sr = new StreamReader(sourceFilePath);
            sw = new StreamWriter(resultFilePath);

            //ReadErrorsTable();
            textPosition.numberLine = 1;
            textPosition.position = -1;

            if (!sr.EndOfStream)
            {
                buf = sr.ReadLine();
                emptyBuf = false;
                NextCh();
            }
            //while (!emptyBuf)
            //    litera = NextCh();

            //StreamWriter swKeys = new StreamWriter("keys.txt");
            //if(!sr.EndOfStream)
            //{
            //    buf = sr.ReadLine();
            //    emptyBuf = false;
            //    NextCh();
            //}
            //while (!emptyBuf)
            //{
            //    symbol = NextSym();
            //    swKeys.WriteLine(symbol.code + " " + symbol.numberLine + " " + symbol.numberPosition);
            //}

            //swKeys.Close();

            if (!emptyBuf)
            {
                symbol = null;
                NextSym();
                initialSets();
                programme();
            }

            if (errorsPositions.Count > 0)
            {
                ListLine();
                if (HasErrors())
                    ListErrors();
            }

            if (errorsPositions.Count > 0)
            {
                oldbuf = buf;
                textPosition.numberLine++;
                ListLine();
                ListErrors(errorsPositions[0].numberLine);
            }

            sr.Close();
            sw.Close();

            WriteNames();
        }

        private void WriteNames()
        {
            using(StreamWriter sw = new StreamWriter("names.txt"))
            {
                foreach (string name in Keys.names)
                    sw.WriteLine(name);
            }
        }

        private void ReadErrorsTable()
        {
            using (StreamReader sr = new StreamReader("Errors.txt"))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    var arr = line.Split(' ');
                    TextPosition textPosition;
                    textPosition.numberLine = Convert.ToInt32(arr[0]);
                    textPosition.position = Convert.ToInt32(arr[1]);
                    Error(arr[2], textPosition);
                }
            }
        }
    }
}
