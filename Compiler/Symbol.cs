﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    class Symbol
    {
        public int code { get; private set; }
        public int numberLine { get; private set; }
        public int numberPosition { get; private set; }

        public Symbol(int code, int numberLine, int numberPosition)
        {
            this.code = code;
            this.numberLine = numberLine;
            this.numberPosition = numberPosition;
        }
    }
}
